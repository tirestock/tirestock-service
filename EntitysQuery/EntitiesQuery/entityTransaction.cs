﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Common;
using System.Data.Objects;
using System.Web;

/// <summary>
/// Summary description for entityTransaction
/// </summary>
public static class entityTransaction
{
    public static void saveTransaction(this ObjectContext db, Func<int> function)
    {
        db.Connection.Open();
        using (DbTransaction tr = db.Connection.BeginTransaction())
        {
            try
            {
                if (function() > 0)
                {
                    tr.Commit();
                }
            }
            catch (Exception) 
            { 
                tr.Rollback(); 
                throw; 
            }
            finally 
            { 
                if (db.Connection.State == 
                    System.Data.ConnectionState.Open) 
                { 
                    db.Connection.Close(); 
                } 
            }
        }
    }
}
