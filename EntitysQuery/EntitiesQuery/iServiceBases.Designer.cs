﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
namespace EntitiesQuery
{
    #region Contexts
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    public partial class iServicesEntities : ObjectContext
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new iServicesEntities object using the connection string found in the 'iServicesEntities' section of the application configuration file.
        /// </summary>
        public iServicesEntities() : base("name=iServicesEntities", "iServicesEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new iServicesEntities object.
        /// </summary>
        public iServicesEntities(string connectionString) : base(connectionString, "iServicesEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new iServicesEntities object.
        /// </summary>
        public iServicesEntities(EntityConnection connection) : base(connection, "iServicesEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region Partial Methods
    
        partial void OnContextCreated();
    
        #endregion
    
        #region ObjectSet Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<t_wh_stock_in> t_wh_stock_in
        {
            get
            {
                if ((_t_wh_stock_in == null))
                {
                    _t_wh_stock_in = base.CreateObjectSet<t_wh_stock_in>("t_wh_stock_in");
                }
                return _t_wh_stock_in;
            }
        }
        private ObjectSet<t_wh_stock_in> _t_wh_stock_in;

        #endregion

        #region AddTo Methods
    
        /// <summary>
        /// Deprecated Method for adding a new object to the t_wh_stock_in EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddTot_wh_stock_in(t_wh_stock_in t_wh_stock_in)
        {
            base.AddObject("t_wh_stock_in", t_wh_stock_in);
        }

        #endregion

        #region Function Imports
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectResult<Nullable<global::System.DateTime>> GetDateServer()
        {
            return base.ExecuteFunction<Nullable<global::System.DateTime>>("GetDateServer");
        }

        #endregion

    }

    #endregion

    #region Entities
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="iServiceBasesModel", Name="t_wh_stock_in")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class t_wh_stock_in : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new t_wh_stock_in object.
        /// </summary>
        /// <param name="serial_number">Initial value of the serial_number property.</param>
        public static t_wh_stock_in Createt_wh_stock_in(global::System.String serial_number)
        {
            t_wh_stock_in t_wh_stock_in = new t_wh_stock_in();
            t_wh_stock_in.serial_number = serial_number;
            return t_wh_stock_in;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String serial_number
        {
            get
            {
                return _serial_number;
            }
            set
            {
                if (_serial_number != value)
                {
                    Onserial_numberChanging(value);
                    ReportPropertyChanging("serial_number");
                    _serial_number = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("serial_number");
                    Onserial_numberChanged();
                }
            }
        }
        private global::System.String _serial_number;
        partial void Onserial_numberChanging(global::System.String value);
        partial void Onserial_numberChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String vignette_no
        {
            get
            {
                return _vignette_no;
            }
            set
            {
                Onvignette_noChanging(value);
                ReportPropertyChanging("vignette_no");
                _vignette_no = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("vignette_no");
                Onvignette_noChanged();
            }
        }
        private global::System.String _vignette_no;
        partial void Onvignette_noChanging(global::System.String value);
        partial void Onvignette_noChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String dot
        {
            get
            {
                return _dot;
            }
            set
            {
                OndotChanging(value);
                ReportPropertyChanging("dot");
                _dot = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("dot");
                OndotChanged();
            }
        }
        private global::System.String _dot;
        partial void OndotChanging(global::System.String value);
        partial void OndotChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Decimal> weight
        {
            get
            {
                return _weight;
            }
            set
            {
                OnweightChanging(value);
                ReportPropertyChanging("weight");
                _weight = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("weight");
                OnweightChanged();
            }
        }
        private Nullable<global::System.Decimal> _weight;
        partial void OnweightChanging(Nullable<global::System.Decimal> value);
        partial void OnweightChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String owner
        {
            get
            {
                return _owner;
            }
            set
            {
                OnownerChanging(value);
                ReportPropertyChanging("owner");
                _owner = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("owner");
                OnownerChanged();
            }
        }
        private global::System.String _owner;
        partial void OnownerChanging(global::System.String value);
        partial void OnownerChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String ctj_inv
        {
            get
            {
                return _ctj_inv;
            }
            set
            {
                Onctj_invChanging(value);
                ReportPropertyChanging("ctj_inv");
                _ctj_inv = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("ctj_inv");
                Onctj_invChanged();
            }
        }
        private global::System.String _ctj_inv;
        partial void Onctj_invChanging(global::System.String value);
        partial void Onctj_invChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String t_number
        {
            get
            {
                return _t_number;
            }
            set
            {
                Ont_numberChanging(value);
                ReportPropertyChanging("t_number");
                _t_number = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("t_number");
                Ont_numberChanged();
            }
        }
        private global::System.String _t_number;
        partial void Ont_numberChanging(global::System.String value);
        partial void Ont_numberChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String test_request
        {
            get
            {
                return _test_request;
            }
            set
            {
                Ontest_requestChanging(value);
                ReportPropertyChanging("test_request");
                _test_request = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("test_request");
                Ontest_requestChanged();
            }
        }
        private global::System.String _test_request;
        partial void Ontest_requestChanging(global::System.String value);
        partial void Ontest_requestChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String tire_size
        {
            get
            {
                return _tire_size;
            }
            set
            {
                Ontire_sizeChanging(value);
                ReportPropertyChanging("tire_size");
                _tire_size = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("tire_size");
                Ontire_sizeChanged();
            }
        }
        private global::System.String _tire_size;
        partial void Ontire_sizeChanging(global::System.String value);
        partial void Ontire_sizeChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String li_and_si
        {
            get
            {
                return _li_and_si;
            }
            set
            {
                Onli_and_siChanging(value);
                ReportPropertyChanging("li_and_si");
                _li_and_si = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("li_and_si");
                Onli_and_siChanged();
            }
        }
        private global::System.String _li_and_si;
        partial void Onli_and_siChanging(global::System.String value);
        partial void Onli_and_siChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String pettern
        {
            get
            {
                return _pettern;
            }
            set
            {
                OnpetternChanging(value);
                ReportPropertyChanging("pettern");
                _pettern = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("pettern");
                OnpetternChanged();
            }
        }
        private global::System.String _pettern;
        partial void OnpetternChanging(global::System.String value);
        partial void OnpetternChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String location
        {
            get
            {
                return _location;
            }
            set
            {
                OnlocationChanging(value);
                ReportPropertyChanging("location");
                _location = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("location");
                OnlocationChanged();
            }
        }
        private global::System.String _location;
        partial void OnlocationChanging(global::System.String value);
        partial void OnlocationChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String from_location
        {
            get
            {
                return _from_location;
            }
            set
            {
                Onfrom_locationChanging(value);
                ReportPropertyChanging("from_location");
                _from_location = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("from_location");
                Onfrom_locationChanged();
            }
        }
        private global::System.String _from_location;
        partial void Onfrom_locationChanging(global::System.String value);
        partial void Onfrom_locationChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Int32> confidential
        {
            get
            {
                return _confidential;
            }
            set
            {
                OnconfidentialChanging(value);
                ReportPropertyChanging("confidential");
                _confidential = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("confidential");
                OnconfidentialChanged();
            }
        }
        private Nullable<global::System.Int32> _confidential;
        partial void OnconfidentialChanging(Nullable<global::System.Int32> value);
        partial void OnconfidentialChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String on_hand
        {
            get
            {
                return _on_hand;
            }
            set
            {
                Onon_handChanging(value);
                ReportPropertyChanging("on_hand");
                _on_hand = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("on_hand");
                Onon_handChanged();
            }
        }
        private global::System.String _on_hand;
        partial void Onon_handChanging(global::System.String value);
        partial void Onon_handChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> receive_date
        {
            get
            {
                return _receive_date;
            }
            set
            {
                Onreceive_dateChanging(value);
                ReportPropertyChanging("receive_date");
                _receive_date = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("receive_date");
                Onreceive_dateChanged();
            }
        }
        private Nullable<global::System.DateTime> _receive_date;
        partial void Onreceive_dateChanging(Nullable<global::System.DateTime> value);
        partial void Onreceive_dateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> create_date
        {
            get
            {
                return _create_date;
            }
            set
            {
                Oncreate_dateChanging(value);
                ReportPropertyChanging("create_date");
                _create_date = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("create_date");
                Oncreate_dateChanged();
            }
        }
        private Nullable<global::System.DateTime> _create_date;
        partial void Oncreate_dateChanging(Nullable<global::System.DateTime> value);
        partial void Oncreate_dateChanged();

        #endregion

    
    }

    #endregion

    
}
