﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesTemp.Masters.Zone
{
    public class ZoneEntity
    {
        public string zone_id { get; set; }
        public string wh_master_id { get; set; }
        public string wh_id { get; set; }
        public string zone { get; set; }
        public string description { get; set; }
        public string zone_type { get; set; }
        public string value_member { get; set; }
        public string is_active { get; set; }
        public DateTime create_date { get; set; }
        public string create_by { get; set; }
        public byte[] rowversion { get; set; }
    }
}