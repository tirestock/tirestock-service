﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesTemp.Masters.Warehouse
{
    public class WarehouseEntity
    {
        public string wh_master_id { get; set; }
        public string wh_id { get; set; }
        public string wh_name { get; set; }
        public string wh_group { get; set; }
        public string decription { get; set; }
        public string addr_line_1 { get; set; }
        public string addr_line_2 { get; set; }
        public string addr_line_3 { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string postal_code { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string user_def1 { get; set; }
        public string user_def2 { get; set; }
        public string user_def3 { get; set; }
        public string user_def4 { get; set; }
        public double? user_def5 { get; set; }
        public double? user_def6 { get; set; }
        public string is_active { get; set; }
        public DateTime create_date { get; set; }
        public string create_by { get; set; }
        public byte[] rowversion { get; set; }
    }
}
