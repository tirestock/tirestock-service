﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesTemp.Temp
{
    public class DataCombobox
    {

        string _displayMember;
        string _valueMember;
        object _dataSource;
        object _value1;

        public string displayMember
        {
            get { return _displayMember; }
            set { _displayMember = value; }
        }
        public string valueMember
        {
            get { return _valueMember; }
            set { _valueMember = value; }
        }
        public object dataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }
        public object value1
        {
            get { return _value1; }
            set { _value1 = value; }
        }
    }
}