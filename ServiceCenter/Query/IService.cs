﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using EntitiesQuery;
using System.ServiceModel.Web;
using System.Data.SqlClient;
using System.Text;
using EntitiesTemp.Masters.Zone;

using VariableTemps.TempGroupUsers;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
[ServiceContract]
public interface IService
{
    /// <summary>
    /// interface IService
    /// </summary>
    /// <param name="composite"></param>
    /// <returns></returns>

    #region "GetDataUsingDataContract"

    [OperationContract]
    CompositeType GetDataUsingDataContract(CompositeType composite);

    #endregion

    #region "HelloWorld"

    //Hello world
    [OperationContract]
    string HelloWorld();

    #endregion

    #region Test Connection

    //Test connection
    [OperationContract]
    object ConnectDB();
    [OperationContract]
    object SqlConn();

    #endregion

    #region "Get DateTime Server"

    [OperationContract]
    object getDateTimeServer();

    #endregion

    #region "Warehouse Stock In"

    //Check serial number is duplicate in system.
    [OperationContract]
    object checkSerialDuplicate(string _serail);

    //Transaction
    [OperationContract]
    object Save(StockInEntity _ent);

    #endregion
}

/// /// <summary>
/// Use a data contract as illustrated in the sample below to add composite types to service operations.
/// </summary>
/// <param name="composite"></param>
/// <returns></returns>
#region "Example DataContract"

[DataContract]
public class CompositeType
{
    bool boolValue = true;
    string stringValue = "Hello ";

    [DataMember]
    public bool BoolValue
    {
        get { return boolValue; }
        set { boolValue = value; }
    }

    [DataMember]
    public string StringValue
    {
        get { return stringValue; }
        set { stringValue = value; }
    }
}

#endregion

#region "DataContract t_wh_stock_in"

[DataContract]
public class StockInEntity
{
    [DataMember]
    public string serial_number { get; set; }
    [DataMember]
    public string vignette_no { get; set; }
    [DataMember]
    public string dot { get; set; }
    [DataMember]
    public object weight { get; set; }
    [DataMember]
    public string owner { get; set; }
    [DataMember]
    public string ctj_inv { get; set; }
    [DataMember]
    public string t_number { get; set; }
    [DataMember]
    public string test_request { get; set; }
    [DataMember]
    public string tire_size { get; set; }
    [DataMember]
    public string li_and_si { get; set; }
    [DataMember]
    public string pettern { get; set; }
    [DataMember]
    public string location { get; set; }
    [DataMember]
    public string from_location { get; set; }
    [DataMember]
    public object confidential { get; set; }
    [DataMember]
    public string on_hand { get; set; }
    [DataMember]
    public object receive_date { get; set; }
    [DataMember]
    public object create_date { get; set; }
}

#endregion