﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq.Dynamic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Data.SqlClient;
using System.ServiceModel.Web;
using System.Data.Entity;

using EntitiesQuery;
using EntitiesTemp.Masters.Zone;
using EntitiesTemp.Temp;
using WorkData;
using Services.FunctionServers;
using TransactionObjects;
using VariableTemps.TempGroupUsers;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{
    public CompositeType GetDataUsingDataContract(CompositeType composite)
    {
        if (composite == null)
        {
            throw new ArgumentNullException("composite");
        }
        if (composite.BoolValue)
        {
            composite.StringValue += "Suffix";
        }
        return composite;
    }

    #region "HelloWorld"

    //Hello world
    public string HelloWorld()
    {
        try { return "HelloWorld"; }
        catch (Exception) { throw; }
    }

    #endregion

    #region "Test Connection"

    //Test connection
    public object ConnectDB()
    {
        SqlConnection conn = (SqlConnection)SqlConn();
        Boolean Result = false;
        try
        {
            conn.Open();
            Result = true;
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            conn.Dispose();
        }
        return Result;
    }
    public object SqlConn()
    {
        string strConn = System.Configuration.ConfigurationManager.ConnectionStrings["iServicesEntities"].ConnectionString;
        int StartIndex = strConn.IndexOf("Data Source"); 
        int StartIndexs = strConn.IndexOf("MultipleActiveResultSets");
        string sub = strConn.Substring(StartIndex, StartIndexs - StartIndex);
        SqlConnection sql = new SqlConnection(sub); 
        return sql;
    }

    #endregion

    #region "Get DateTime Server"

    public object getDateTimeServer()
    {
        using (iServicesEntities db = new iServicesEntities())
        {
            return db.GetDateServer().First().Value;
        }
    }

    #endregion

    #region "Warehouse Stock In"

    //Check serial number is duplicate in system.
    public object checkSerialDuplicate(string _serail)
    {
        try
        {
            using (iServicesEntities db = new iServicesEntities())
            {
                var serial_number = db.t_wh_stock_in.FirstOrDefault(q => q.serial_number == _serail);
                if (serial_number != null)
                    return true;
                else
                    return false;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    //Transaction
    public object Save(StockInEntity _ent)
    {
        try
        {
            using (iServicesEntities db = new iServicesEntities())
			{
				db.saveTransaction(delegate()
				{
					var _map_wh_stock_in = new t_wh_stock_in();
					_map_wh_stock_in.CloneObject(_ent);
                    db.t_wh_stock_in.AddObject(_map_wh_stock_in);
					return db.SaveChanges();
				});
				return true;
			}
        }
        catch (Exception)
        {
            throw;
        }
    }

    #endregion
}