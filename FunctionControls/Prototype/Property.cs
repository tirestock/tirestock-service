﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

[DataContract]
public class Property
{
    [DataMember]
    public int Id { get; set; }
	[DataMember]
	public string Code { get; set; }
	[DataMember]
	public string Name { get; set; }
	[DataMember]
	public string Description { get; set; }
}