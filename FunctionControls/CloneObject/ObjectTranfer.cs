﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CloneObject
/// </summary>
public static class ObjectTranfer
{
	public static void CloneObject(this object _target, object _source)
        {
            if ((_target == null) || (_source == null)) return;
            var propertyList = System.ComponentModel.TypeDescriptor.GetProperties(_target);
            foreach (var prop in _target.GetType().GetProperties())
            {
                var propertyDesc = propertyList.Find(prop.Name, true);
                if (propertyDesc != null)
                {
                    try
                    {
                        object sourceValue = _source.GetPropertyValue(prop);
                        if (sourceValue != null)
                            propertyDesc.SetValue(_target, sourceValue);
                    }
                    catch { }
                }
            }
        }

	public static void SetPropertyValue(this object _component, string _propertyName, object _value)
        {
            if (_component == null) return;
            var propertyList = System.ComponentModel.TypeDescriptor.GetProperties(_component);

            var prop = _component.GetType().GetProperties().FirstOrDefault(qry => qry.Name == _propertyName);
            if (prop != null)
            {
                var propertyDesc = propertyList.Find(prop.Name, true);
                if (propertyDesc != null)
                    propertyDesc.SetValue(_component, _value);
            }
        }
    public static object GetPropertyValue(this object _component, string _propertyName)
        {
            if (_component == null) return null;
            var propertyList = System.ComponentModel.TypeDescriptor.GetProperties(_component);

            var prop = _component.GetType().GetProperties().FirstOrDefault(qry => qry.Name == _propertyName);
            if (prop != null)
            {
                var propertyDesc = propertyList.Find(prop.Name, true);
                if (propertyDesc!= null)
                    return propertyDesc.GetValue(_component);
            }
            return null;
        }
    public static object GetPropertyValue(this object _component, System.Reflection.PropertyInfo _propertyInfo)
        {
            if (_component == null) return null;
            var propertyList = System.ComponentModel.TypeDescriptor.GetProperties(_component);
            var prop = _component.GetType().GetProperties().FirstOrDefault(qry => qry.Name == _propertyInfo.Name);
            if (prop != null)
            {
                var propertyDesc = propertyList.Find(prop.Name, true);
                if (propertyDesc != null)
                    return propertyDesc.GetValue(_component);

            }
            return null;
        }
}