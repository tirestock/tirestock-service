﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;

namespace Services.FunctionServers
{
    public class CustomExecute
    {
        public static int ExecuteSql(System.Data.Entity.DbContext context, string sql)
        {
            int complete = 0;

            var entityConnection = (System.Data.SqlClient.SqlConnection)context.Database.Connection;

            DbConnection conn = entityConnection;
            ConnectionState initialState = conn.State;

            try
            {
                if (initialState != ConnectionState.Open)
                    conn.Open();  // open connection if not already open
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                }

                complete = 1;
            }
            catch (Exception ex)
            {
                throw new Exception("Execute Exception : \n" + ex.InnerException.Message);
            }
            finally
            {
                if (initialState != ConnectionState.Open)
                    conn.Close(); // only close connection if not initially open
            }

            return complete;
        }
    }
}