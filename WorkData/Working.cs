﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Objects;
using System.Data.Entity;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;

namespace WorkData
{
    public static class Work
    {
        public static bool Working(Func<bool> statement)
        {
            bool result = false;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (statement() == true) result = true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    //MsgBox.MSGBox.Error(ex.InnerException.Message.ToString());
                }
                else
                {
                    //MsgBox.MSGBox.Error(ex.Message.ToString());
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            return result;
        }
        public static bool Save(global::System.Data.Entity.DbContext obj, Func<int> statement)
        {
            var result = false;
            System.Data.Common.DbConnection dbConn = obj.Database.Connection;
            dbConn.Open();
            using (System.Data.Common.DbTransaction transaction = obj.Database.Connection.BeginTransaction())
            {
                try
                {
                    if (statement() > 0)
                    {
                        transaction.Commit();
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (dbConn.State == ConnectionState.Open) dbConn.Close();
                }
            }
            return result;
        }
        public static void saveTransaction(global::System.Data.Entity.DbContext db, Func<int> function)
        {
            System.Data.Common.DbConnection dbConn = db.Database.Connection;
            dbConn.Open();
            using (System.Data.Common.DbTransaction tr = db.Database.Connection.BeginTransaction())
            {
                try
                {
                    if (function() > 0)
                    {
                        tr.Commit();
                    }
                }
                catch (Exception) { tr.Rollback(); throw; }
                finally { if (db.Database.Connection.State == System.Data.ConnectionState.Open) { db.Database.Connection.Close(); } }
            }
        }
        public static DataTable ToTable<T>(IEnumerable<T> data)
        {
            var table = new System.Data.DataTable();

            try
            {
                PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }

                object[] values = new object[props.Count];

                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item) ?? DBNull.Value;
                    }
                    table.Rows.Add(values);
                }
            }
            catch
            {
                throw;
            }

            return table;
        }
    }
}